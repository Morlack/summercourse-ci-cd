# Chapter 2: Continuous Deployment

Continuous Deployment means that we are deploying our code as often as possible.
Automatisation is in this case really the way to go, or you can spend a lot of time continuously deploying the code.

For the sake of time we will only be automatically deploying the frontend to an S3 bucket.   

## Step 1: Building our Frontend
To deploy our frontend we are first going to have to build it.
 
The commands to build the frontend are:
```bash
cd frontend
npm install
npm run build
```

### Assignment
Define a job which uses the above CI script to build the frontend. 
Use a new `build` stage for this, since it only should be executed when the tests succeed.


## Step 1: Building our Backend
We will also need our backend later on.
 
The commands to build the backend are:
```bash
cd backend
mvn install -B -DskipTests
```

### Assignment
Define a job which uses the above CI script to build the backend. 
Use a new `build` stage for this, since it only should be executed when the tests succeed.

## Step 3: Using artifacts
Every pipeline is producing so-called artifacts. This can be:
- Build results (executables, html, js)
- Test Coverage reports
- Information needed in the next step

Most companies have a central artifact repository, like Artifactory or Nexus.
Gitlab has artifact management built-in, so we are going to use that. 

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#artifacts

### Assignment
Consult the above resource to export artifacts of the frontend (`frontend/dist/*`) and backend (`backend/target/*.jar`) and download them to your PC afterwards.



## Step 4: Secrets
We want to deploy our code to AWS S3. But unless we are going to make the S3 Bucket publicly writable (which is very advisable against!) you are going to need credentials in your pipeline.

If you're unfamiliar with AWS, you can set two environment variables so the aws-cli knows how to authenticate.

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

### Resources
- https://gitlab.com/help/ci/variables/README#variables

### Assignment
Set those variables (ask for the value) using the gitlab Variables section in your CI/CD Settings

## Step 5: Deploying to S3
We can use the artifacts generated in step 2 to deploy them to S3! 
The script to upload the frontend build to S3 looks like this:

```bash
S3_USERNAME="your_username"
aws s3 cp frontend/dist/* s3://summercourse-ci-cd-web-deployment/${S3_USERNAME}/ --recursive
echo "You can find your summercourse website on http://summercourse-ci-cd-web-deployment.s3-website-eu-west-1.amazonaws.com/$S3_USERNAME"
```
For the script to succeed we will first need the artifacts from the build step...

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#dependencies

### Assignment
- Create a job using the above script in a new stage (`deploy`)
- Retrieve the artifact files in the job using the `dependencies` definition. (docs in Resources)
## End of Chapter
You have successfully made it through the second chapter! You can compare your results with the `results/chapter-2` branch now!
